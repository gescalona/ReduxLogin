import {ACCESS_LOGIN, ACCESS_SING_OUT} from '../constants';

export function singOut(transition) {
  const action = {
    type: ACCESS_SING_OUT,
    profilePatient: null,
    status: null,
    transition
  }
  return action;
}

export function login(res) {
  let action = {};

  if(res.data !== undefined){
    action = {
      type: ACCESS_LOGIN,
      profilePatient: JSON.parse(res.data.profile),
      status: 200

    }
  }else if(res.code === 401){
    action = {
      type: ACCESS_LOGIN,
      profilePatient: null,
      status: 401

    }
  }else {
    action = {
      type: ACCESS_LOGIN,
      profilePatient: null,
      status: 500
    }
  }
  return action;
}


export function loginFetch(username, password) {

  return async function(dispatch) {

    const data = {
      _username: username,
      _password: password
    };
    const json = JSON.stringify(data);
    await fetch('http://api.cuidados24.com/api/login_check', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "Accept": 'application/json'
      },
      body: json
    })
    .then(
      u => {
          return u.json();
    })
    .then(res => {
      console.log(res);
      dispatch(login(res));
    });

  }
}

export function singOutFetch(transition) {

  return async function(dispatch) {

      console.log(transition);
      dispatch(singOut(transition));

  }
}
