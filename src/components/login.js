import React, {Component} from 'react'
import {View, Text, StyleSheet, TextInput, TouchableOpacity, ScrollView, KeyboardAvoidingView } from 'react-native'
import { loginFetch } from '../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';


const appStyles = require('../assets/styles');
const styles = Object.assign({}, appStyles, StyleSheet.create({}));

class login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'cliente@gmail.com',
      password: '123456'
    };
  }
  async handleLogin(){

    if (this.state.username !== '' && this.state.username !== null && this.state.password !== '' && this.state.password !== null) {

      await this.props.loginFetch(this.state.email,this.state.password);
      if(this.props.loginRes != null){

        if(this.props.loginRes.status === 200){
          Actions.home();
        }else if(this.props.loginRes.status === 401) alert("error en la clave")
        else if(this.props.loginRes.status === 500) alert("error en la clave")
      }

    }else{
      alert("Campos vacios");
    }
  }


  render(){
    console.log(this.props);
    return(
        <ScrollView style={appStyles.contenedor}>
          <View style={{flex: 1}}></View>
            <KeyboardAvoidingView
              keyboardVerticalOffset={-100}
              behavior={'padding'}
              style={[appStyles.formStyle]}
            >
          <View style={appStyles.flex1}>
            <View style={appStyles.flex1}>
              <TextInput
                  style={appStyles.input}
                  value={this.state.email}
                  placeholder={"Email"}
                  onChangeText={email => this.setState({ email })}
                />
            </View>

            <View style={appStyles.flex1}>
              <TextInput
                  style={appStyles.input}
                  placeholder={"password"}
                  value={this.state.password}
                  secureTextEntry={true}
                  onChangeText={password => this.setState({ password })}
                />
            </View>

            <View style={appStyles.flex1}>
               <TouchableOpacity onPress={() => this.handleLogin() } style={appStyles.button} >
                 <Text style={appStyles.label}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex: 1}}></View>
          </KeyboardAvoidingView>
        </ScrollView>
    )
  }
}



function mapStateToProps(state){
  return {
    loginRes : state.login
  };
}

export default connect(mapStateToProps, {loginFetch})(login);
