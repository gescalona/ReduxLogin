import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import { loginFetch } from '../actions';
import { connect } from 'react-redux';


const appStyles = require('../assets/styles');
const styles = Object.assign({}, appStyles, StyleSheet.create({}));
class home extends Component {
  render(){
    return(
        <View  style={appStyles.contenedor}>
          <View  style={[ appStyles.home, {flex: 1}]} >
            <Text style={appStyles.label}>HOME</Text>
          </View>
          <View  style={[ appStyles.back, {flex: 1}]}>
            <Text style={appStyles.label}>LOGOUT</Text>
          </View>
        </View>
    )
  }
}

export default home;
