import { ACCESS_LOGIN, ACCESS_SING_OUT } from '../constants';

const login = (action) => {  return {
    profile : action.profile,
    status: action.status
  }
}
const logout = (action) => {  return {
    profile : action.profile,
    status: action.status
  }
}

const loginRes =  (state = null, action) => {
  let loginRes = null;
  switch (action.type) {
    case ACCESS_LOGIN:
        loginRes = login(action)
      return loginRes;
    case ACCESS_SING_OUT:
      loginRes = logout(action)
      action.transition.router.stateService.go('login');
        return loginRes
    default:
      return state;
  }

}

export default loginRes;
