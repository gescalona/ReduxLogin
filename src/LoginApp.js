import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {connect} from 'react-redux'
import { Router, Scene } from 'react-native-router-flux';

import Login from './components/login';
import Home from './components/home';

const RouterWithRedux = connect()(Router);

class LoginApp extends Component {
    getSuperheroes(){
        const {superheroes} = this.props.loginRes

    }
    render(){
      console.log(this.props);
        return (
          <RouterWithRedux>
            <Scene key="root">
              <Scene key="login" component={Login} hideNavBar initial />
              <Scene key="home" component={Home} />
            </Scene>
          </RouterWithRedux>
        )
    }
}

export default LoginApp
