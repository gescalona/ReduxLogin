import React from 'react'
import {View, Text} from 'react-native'
import {Provider} from 'react-redux'

import LoginApp from './LoginApp';
import thunk from 'redux-thunk';

import configureStore from './configureStore';
let store = configureStore(thunk);
/* Aquí no pongo el return porque estoy usando ES6 */
const App = () => (
    <Provider store={store}>
      <LoginApp />
    </Provider>
)

export default App
