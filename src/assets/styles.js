var {StyleSheet, Platform} = require('react-native');

import metrics from './metrics';

module.exports = StyleSheet.create({
  contenedor:{
    flex: 1,
    flexDirection: 'column',
    backgroundColor: "#4d4d4d",
    padding:20

  },
  label:{
    color: "#FFFFFF",
    fontWeight: 'bold'
  },
  input:{
    marginTop:5,
    height: 40,
    flex:1,
    backgroundColor: "#FFFFFF"
  },
  flex1:{
    flex:1
  },
  scrollView: {
    flex: 1,
  },
  button:{
    flex: 1,
    backgroundColor:"#2FAC00",
    padding:20,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:10
  },
  formStyle: {
    marginTop: 10,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    width: metrics.DEVICE_WIDTH,
    height: metrics.DEVICE_HEIGHT,
    paddingTop: 24,
    backgroundColor: 'white'
  },
  home:{
    flex: 1,
    backgroundColor: "#165FE6",
    alignItems: 'center',
    justifyContent: 'center'
  },
  back:{
    flex: 1,
    backgroundColor: "#FF5600",
    alignItems: 'center',
    justifyContent: 'center'
  }

})
